# 1. Create a function called 'is_prime' that takes an integer as an argument and returns True if it's a prime number and False otherwise.
import math
def isprime(num):
    for i in range (2,int(math.sqrt(num))+1):
        if num%i==0:
           return False
    else:
        return True
print(isprime(5)) 

# 2. Implement a function called 'generate_fibonacci'  that generates the Fibonacci sequence up to a given limit.
#  The function should return a list of Fibonacci numbers.

def fibonacci(f_num):     
    FibArray = [0,1]
    if(f_num<=0):
        return "Incorrect Value"
    if(f_num==1):
        return FibArray[0]
    if(f_num==2):
        return FibArray 
    if(f_num>2):
        f=2
        while(f!=f_num):
            FibArray.append(FibArray[f - 1] + FibArray[f - 2])
            f+=1
    return FibArray
print("Fibonacci sequence list: ",fibonacci(5))
fib = [0, 1]           # Version 2
for i in range(2,5):
    fib.append(fib[i-1]+fib[i-2])
print("Fibonacci sequence list: ",fib)

# 3. Define a function 'sum_of_digits' that takes an integer as input and returns the sum of its digits. 
# This problem involves iterating through the digits of a number.


def sum_of_digits(n):
    sum=0
    while n!=0:
        rem=n%10
        sum+=rem
        n=n//10
    return sum 
print(sum_of_digits(51))   

# 4. Implement a function to find the kth largest element in an unsorted array.
def list_k_largest(ls,k):
    max1=ls[0]
    max2=ls[1]
    max3=ls[2]
    for i in ls:
        if i>max3:
            max3=i
        if max3>max2:
            temp=max3
            max3=max2
            max2=temp
        if max2>max1:
            temp1=max2
            max2=max1
            max1=temp1
    return max3
ls=[-1,2,5,7,15,14,8]
print(list_k_largest(ls,3))

def largest_elem(list1,k):
    for i in range(len( list1)):
        for j in range(i+1,len(list1)):
            if list1[i]>list1[j]:
                list1[i],list1[j]=list1[j],list1[i]
                
    return list1[-k]
lst=[1,5,-2,7,0,15,6,17]
k=5
print(f"The {k}-th largest element in the list is: ",largest_elem(lst,k))            

# 5. Implement a function to determine if a given string is a valid number

def is_digit(n_):
    if n_.isdigit():
        return True
    else:
        return False
print(is_digit('1A7')) 

# 6. Given a roman numeral, convert it to an integer.

def roman_to_numeral(r):
    dict_={'I':1,
        'V':5,
        'X':10,
        'L':50,
        'C':100,
        'D':500,
        'M':1000,
    }
    numeric=0
    for i in range(len(r)):
        if i>0 and dict_[r[i]]>dict_[r[i-1]]: 
            numeric+=dict_[r[i]]-2*dict_[r[i-1]]
        else:
            numeric+=dict_[r[i]]    
    return numeric             
inp=input("Please enter a roman number to convert it to numeral: ")
print(roman_to_numeral(inp))    

# 7. Given an integer array nums, find the maximum product of two distinct elements in the array

arr=[15,7,1,0,2,10]

max_prod=arr[0]*arr[1]
for i in range(len(arr)):
    for j in range(1,len(arr)):
        if max_prod< arr[i]*arr[j]:
            max_prod=arr[i]*arr[j]
print(max_prod)            

#8. Given an m x n matrix, if an element is 0, set its entire row and column to 0. Do it in-place.

def matrix_transform(matrix):
    for i in range(len(matrix)):
        for j in range(len(matrix[0])):
            if matrix[i][j]==0:
                for k in range(len(matrix[0])):
                    if k!=j:
                        matrix[i][k]="*"
                for n in range(len(matrix)):
                    if n!=i:
                        matrix[n][j]="*" 
                
    for i in range(len(matrix)):
        for j in range(len(matrix[0])):
            if matrix[i][j]=="*":
                matrix[i][j]=0
    return matrix  
matrix=[[1,2,5,3],
        [4,0,5,7],
        [8,9,10,11]]    
print(matrix_transform(matrix))       
